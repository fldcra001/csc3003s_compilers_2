/*
 * Performs depth first transversal on parse tree
 * Outputs the AST to console and disk
 *
 * Craig Feldman
 * 18 September 2014
 */

import nulang.node.*;
import nulang.analysis.*;

public class Printer extends DepthFirstAdapter {
	
	// how much we need to indent when printing
	int indent;
	
	//the string that will be outputted to console
	StringBuilder toOutput;
	
	public Printer() {
		toOutput = new StringBuilder();
	}
	
	//returns a string containing the indent
	private void indent() {
		toOutput.append("\n");
		for (int i = 0; i < indent; ++i)
			toOutput.append("\t");
	}
	
	@Override
	public void defaultCase (Node node) {
		indent();
		
		if (node instanceof TIdentifier)
			toOutput.append("ID," + node);
		
		else if (node instanceof TFloat)
			toOutput.append("FLOAT_LITERAL," + node);
	}
	

	@Override
	public void defaultIn(Node node) {
		indent();
		printNode(node);
		++indent;
	}

	private void printNode(Node node) {
		String name = node.getClass().getSimpleName();
		if (!name.equals("Start"))
			name = name.substring(1);
		
		toOutput.append(name);
	}

	@Override
	public void defaultOut(Node node) {
		--indent;
	}

	public String getOutput() {
		return toOutput.toString();
	}
	
}
