##############################
CSC3003S Compilers – Assignment 2: 
Abstract Syntax Tree, Semantic Analysis and Error Reporting
README.txt

Craig Feldman

22 September 2014
#############################

------------
Instructions
------------
Extract the contents of the archive to disk. 

Run the command 'java -jar lib/sablecc.jar grammar.txt' to generate SableCC files.
Run the command 'javac Errors.java Parse.java SemanticAnalyser.java Printer.java' to generate class files. 

To generate the AST, use the command "java Parse [file.mnl]".
To check an mnl file for errors, use the command "java Errors [file.mnl]".
The output will be displayed to console and written to disk (in the ast_outputs or error_outputs folder)

-----
Note
-----
This application was developed using JavaSE-1.7 and SableCC v3.7 under Windows 8.
The application has been tested on Linux.

I have included the test files in two folders, 'lexical_samples' and 'parse_samples'.
To run the application using these files, you need to provide the file path, eg. java Parse parse_samples/expression.mnl

Outputs are written to console and disk.
AST outputs are written to the folder ast_outputs.
Error outputs are written to the folder error_outputs.

The grammar file is included ('grammar.txt'). 


Enjoy!


-------------------------------
Assignment Instructions
-------------------------------

Introduction

This assignment will follow on from assignment 1. You will be expected to create an Abstract Syntax Tree (AST), do some basic Semantic Analysis and Error Reporting for a made-up programming language called mini nulang, which is based on a reduced grammar for nulang (excluding functions).

The Syntactic Analyser (parser) program from Assignment 1 should be modified to take mini nulang files (*.mnl) and should generate an appropriate Abstract Syntax Tree which is output to the screen and to a corresponding *.ast file.

The Abstract Syntax Tree should then be analysed to do simple semantic analysis using a basic Symbol Table stored in a simple data structure, and then report on any errors which should be output to the screen and also to a corresponding *.err file. Errors from previous stages, lexical analysis and syntactic analysis, should also be output to the screen and written to the *.err file.

Tools

The programming language and compiler tools for this assignment can be either

Java with SableCC, JavaCC, or JFlex and Cup
Python with PLY
C++ with Flex and Bison
Although it is strongly recommended that you use Java with SableCC, or Python with PLY, since that is what the reference solution(s) will be implemented in and they have established methods for generating an Abstract Syntax Tree.

Input, Output and Testing

The input *.mnl source code file should be specified as a command line parameter when your programs are run, e.g.

                parse  my_program.mnl

                errors my_program.mnl

The parser should as before check the tokens conform to the grammar defined below and construct and output an abstract syntax tree as flat text using depth-first traversal, visiting the root, then children from left to right onto the screen and also in a corresponding file, my_program.ast.

The semantic analysis program should follow on from parsing and check the program for basic semantic errors (specified below) and then report the first error identified - whether lexical, syntactic or semantic - outputting it to the screen and also in a corresponding file, my_program.err.

Download the parse_mininulang_samples.zip file containing *.mnl code input files and their corresponding output *.ast files (also some *.str files for comparison purposes), and the error_mininulang_samples.zip file containing *.mnl code input files and their corresponding output *.err files, indicating what the output should look like and can be used to test your program.

Mini Nulang Grammar

The grammar uses the notation N∗, for a non-terminal N, meaning 0, 1, or more repetitions of N. Bold symbols are keywords and should form their own tokens, and other tokens are in italics.

Which nulang grammar rules have been removed can be viewed here: nulang_reduced.

Program               → Statement*                                                   // this asterisk indicates closure

Statement          → identifier = Expression

Expression          → Expression + Term

                                → Expression - Term

                                → Term

Term                    → Term * Factor                                               // this asterisk indicates multiplication

                                → Term / Factor

                                → Factor

Factor                  → ( Expression )

                                → float

                                → identifier

Abstract Syntax Tree

The Abstract Syntax Tree is a reduced version of the Parse (Syntax) Tree with unnecessary nodes like e.g. operators and punctuation removed. Single child nodes can be kept, although strictly they should be removed for a more optimal AST.

Semantic Analysis

Mini Nulang (and Nulang) has the following two properties, it is:

- “dynamically typed”, in the sense that variables do not have to be explicitly created with a particular type, e.g. float val. Once a variable is assigned a value it is considered to be created.

- “functional”, in the sense that variables should not be mutable. So a variable can only legally be assigned a value/expression once. If a variable is assigned another value/expression later on, it should be considered a semantic error, having been already defined.

So your semantic analysis should perform two checks:

If a variable(identifier) is created/defined on the left hand side of an assignment, it should check if it has already been defined, in which case it should generate an appropriate semantic error.
If a variable(identifier) is used in the right hand side of an assignment it should check if it has been defined already, and if not it should generate an appropriate semantic error.
So semantic analysis should be traverse the AST and maintain a simple Symbol Table data structure, which can be a simple data structure, but often is a stack to handle more sophisticated scoping checks for more complex languages.

Error Reporting

If one of the semantic errors specified above, or another error from a previous stage, lexical analysis or syntactic analysis, occurs then this should be output to the screen and to a corresponding *.err file indicating the type of error with some appropriate detail about the error, including the line number, description and the problematic token/identifier/variable. (To simplify the problem we’ll assume the program file has no empty lines.)