/*
 * Parses a mini-nulang file and generates a parse tree according to the specified grammar (grammar.txt)
 * 
 * Makes use of SableCC
 * 
 * Craig Feldman
 * 18 September 2014
 */

//package nulang;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.PushbackReader;

import nulang.lexer.Lexer;
import nulang.lexer.LexerException;
import nulang.node.Start;
import nulang.parser.Parser;
import nulang.parser.ParserException;

public class Errors {

	public static void main(String[] args) throws IOException {
		/*
		if (args.length != 1) {
			System.out.println("Incorrect number of arguments.");
			System.out.println("Proper usage is 'java Parse file.mnl'");
			System.exit(0);
		}
		
		String inputFile = args[0];
		*/
		String inputFile = "error_samples/serror2.mnl";
		String outputFile = "";
		if (inputFile.contains("/"))
			outputFile = "error_outputs" + inputFile.substring(inputFile.lastIndexOf("/"), inputFile.indexOf(".")) + ".err";
		else outputFile = "error_outputs/" + inputFile.substring(0, inputFile.indexOf(".")) + ".err";
		
		FileReader inFile = null;
		PrintWriter outFile = null;
		System.out.println("\nChecking  '" + inputFile +"' for errors...");
		System.out.println("Outputting to console and '" + outputFile +"'\n");
		
		 try {
	            inFile = new FileReader(inputFile);
	    		outFile = new PrintWriter(outputFile);
	    		
	            Lexer lexer = new Lexer(new PushbackReader(new BufferedReader(inFile),1024));    
	            Parser p = new Parser(lexer);
	           
	            Start tree = p.parse();
	            SemanticAnalyser S = new SemanticAnalyser(outFile);	            
	            tree.apply(S);

	            
	        } catch (FileNotFoundException e) {
	        	System.err.println(e.getMessage());
	        //	e.printStackTrace();
	        	
	        } catch (LexerException e){
	        	System.err.println(e);
	        	outFile.write(e.toString());
			} catch (ParserException e){
	        	System.err.println(e);
	        	outFile.write(e.toString());
	        	
			}catch (Exception e) {
	        	System.err.println("Runtime exception!");
	            throw new RuntimeException("\n"+e.getMessage());
	        } finally {
	        	inFile.close();
	        	outFile.close();        	
	        }
	        
	}

}
