import java.io.PrintWriter;
import java.util.Hashtable;

import nulang.analysis.DepthFirstAdapter;
import nulang.node.AAssignStatement;
import nulang.node.AIdentifierExpression;
import nulang.node.TIdentifier;

/*
 * Performs depth first transversal on parse tree to identify semantic errors
 * Outputs the errors to console and disk
 *
 * Craig Feldman
 * 19 September 2014
 */
public class SemanticAnalyser extends DepthFirstAdapter {
	// symbol table to store variable info
	private Hashtable<String, String> symbolTable = new Hashtable<String, String>();
	private PrintWriter outFile;
	
	public SemanticAnalyser(PrintWriter outFile) {
		this.outFile = outFile;
	}
	
	
	// Checks if an identifier has been defined before use
	public void outAIdentifierExpression(AIdentifierExpression node) {
		TIdentifier identifier = node.getIdentifier();
		
		String  key = identifier.getText();

		if (!symbolTable.containsKey(key)) {
			String message = "SemanticException: ";
			message += "[" + identifier.getLine() + "] " + identifier.getText() + " ";
			message += "not defined.";
			
			outFile.write(message);
			System.err.println(message);			
			outFile.close();
			
			System.exit(0);
		}

	}
	
	//  Generates an error if an identifier is being redefined, otherwise adds it to the symbol table
	public void outAAssignStatement(AAssignStatement node) {
		TIdentifier identifier = node.getIdentifier();
		String  key = identifier.getText();

		if (symbolTable.containsKey(key)) {
			String message = "SemanticException: ";
			message += "[" + identifier.getLine() + "] " + identifier.getText() + " ";
			message += "already defined.";
			
			outFile.write(message);
			System.err.println(message);			
			outFile.close();
			
			System.exit(0);
		}
		else
			symbolTable.put(key, key);
	}
	
	
	
}
