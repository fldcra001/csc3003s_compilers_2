/*
 * Parses a mini-nulang file and generates a parse tree according to the specified grammar (grammar.txt)
 * 
 * Makes use of SableCC
 * 
 * Craig Feldman
 * 18 September 2014
 */

//package nulang;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.PushbackReader;

import nulang.lexer.Lexer;
import nulang.lexer.LexerException;
import nulang.node.Start;
import nulang.parser.Parser;
import nulang.parser.ParserException;

public class Parse {

	public static void main(String[] args) throws IOException {
		/*
		if (args.length != 1) {
			System.out.println("Incorrect number of arguments.");
			System.out.println("Proper usage is 'java Parse file.mnl'");
			System.exit(0);
		}
		
		String inputFile = args[0];
		*/
		String inputFile = "parse_samples/statements.mnl";
		String outputFile = "";
		if (inputFile.contains("/"))
			outputFile = "ast_outputs" + inputFile.substring(inputFile.lastIndexOf("/"), inputFile.indexOf(".")) + ".ast";
		else outputFile = "ast_outputs/" + inputFile.substring(0, inputFile.indexOf(".")) + ".ast";
		
		FileReader inFile = null;
		PrintWriter outFile = null;
		
		System.out.println("\nGenerating parse tree for file '" + inputFile +"'");
		System.out.println("Outputting to console and '" + outputFile +"'");
		
		 try {
	            inFile = new FileReader(inputFile);
	    		outFile = new PrintWriter(outputFile);
	    		  		
	            Lexer lexer = new Lexer(new PushbackReader(new BufferedReader(inFile),1024));    
	            Parser p = new Parser(lexer);
	           
	            Start tree = p.parse();
	            
	            //check for semantic errors - exits if error found
	            SemanticAnalyser S = new SemanticAnalyser(outFile);	            
	            tree.apply(S);
	            
	            //print the ast
	            Printer P = new Printer();	            
	            tree.apply(P);
	            
	            String toOutput = P.getOutput();
	            
	            //write output to console and file
	    		System.out.println("\n--------------BEGIN AST--------------");
	            System.out.println(toOutput);
	    		System.out.println("--------------END AST--------------");
	            outFile.write(toOutput);
	            
	        } catch (FileNotFoundException e) {
	        	System.err.println(e.getMessage());
	        //	e.printStackTrace();
	        	
	        } catch (LexerException e){
	        	System.err.println(e);
	        	outFile.write(e.toString());
			} catch (ParserException e){
	        	System.err.println(e);
	        	outFile.write(e.toString());
	        	
			}catch (Exception e) {
	        	System.err.println("Runtime exception!");
	            throw new RuntimeException("\n"+e.getMessage());
	            
	        } finally {
	        	inFile.close();
	        	outFile.close();        	
	        }
	        
	}

}
